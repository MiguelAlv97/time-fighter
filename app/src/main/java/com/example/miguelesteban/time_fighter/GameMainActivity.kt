package com.example.miguelesteban.time_fighter;

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class GameMainActivity : AppCompatActivity() {

    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var tapMeButton: Button
    internal var score = 0

    internal var gameStarted = false
    internal lateinit var countDownTimer: CountDownTimer
    internal val countDownInterval = 1000L
    internal val initialCountDown = 10000L // Cuando son cosntantes usar val en kotlin
    internal var timeLeft = 10
    internal lateinit var ColorLayout: RelativeLayout
    internal val TAG = GameMainActivity::class.java.simpleName
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("timeFighter")



    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private val GAME_STARTED = "GAME_STARTED"

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)

        Log.d(TAG, "onCreate called . Score is $score")
        //conect views to variables
        gameScoreTextView = findViewById(R.id.Game_Score_textView)
        timeLeftTextView = findViewById(R.id.Time_Left_textView)
        tapMeButton = findViewById(R.id.tap_me_button)
        ColorLayout = findViewById(R.id.LayoutColor)


        //var scoreAux = score
        if(savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            gameStarted = savedInstanceState.getBoolean(GAME_STARTED)
            restoreGame()
        }else {
            resetGame()
        }
        tapMeButton.setOnClickListener{ _ -> incrementScore()

        }
    }


    private fun resetGame(){//resetGame(scoreAux: Int){
        //score = scoreAux
        score = 0
        timeLeft = 10

        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = gameScore

        val timeLeftText = getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text = timeLeftText

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }

            override fun onFinish() {
                endGame()
            }
        }

        gameStarted = false

    }

    private fun incrementScore(){
        score++
        //val newScore = "Your score: " + Integer.toString(score)
        val newScore = getString(R.string.your_score, Integer.toString(score))

        gameScoreTextView.text = newScore

        ColorLayout.setBackgroundColor(Color.parseColor(RandomColors()))



        if(!gameStarted){
            startGame()
        }
    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame(){
        ref.child(UUID.randomUUID().toString()).child("Score").setValue(score.toString())
        Toast.makeText(
                this,"Puntaje guardado en la base   " +
                getString(R.string.game_over_message, Integer.toString(score)),
                Toast.LENGTH_LONG).show()

        resetGame()
    }

    fun restoreGame(){
        val restoredScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = restoredScore

        val restoredTime = getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text = restoredTime

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() /1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft.toString())
            }

            override fun onFinish() {
                endGame()
            }

        }
        if (gameStarted){
            countDownTimer.start()
        }

    }

    fun RandomColors():String{
        val colores = arrayOf("#00FF00", "#FF0000","#000000", "#FFFFFF", "#FF00FF")
        val randomInteger = (0..4).shuffled().first()

        return colores.get(randomInteger)
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SCORE_KEY,score)
        outState!!.putInt(TIME_LEFT_KEY,timeLeft)
        outState!!.putBoolean(GAME_STARTED,gameStarted)


        countDownTimer.cancel();
        Log.d(TAG, "onSaveInstanceState: score = $score & timeLeft = $timeLeft")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy called")
    }

}
